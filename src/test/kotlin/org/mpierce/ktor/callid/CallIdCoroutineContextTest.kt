package org.mpierce.ktor.callid

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallId
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.withContext
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.concurrent.Executors

internal class CallIdCoroutineContextTest {
    @Test
    internal fun noCallIdFeatureNoCoroutineContext() {
        val appInit: Application.() -> Unit = {
            callIdEndpoint()
        }

        withTestApplication(appInit) {
            with(handleRequest(HttpMethod.Get, "/call-id")) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals("call id: null", response.content!!)
            }
        }
    }

    @Test
    internal fun callIdFeatureAppliedAndIsInCoroutineContext() {
        val appInit: Application.() -> Unit = {
            install(CallId) {
                generate { "generated-call-id" }
            }
            callIdEndpoint()
        }

        withTestApplication(appInit) {
            with(handleRequest(HttpMethod.Get, "/call-id")) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals("call id: generated-call-id", response.content!!)
            }
            // available when the coroutine has switched to another thread, too
            with(handleRequest(HttpMethod.Get, "/call-id-other-thread")) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals("call id: generated-call-id", response.content!!)
            }
        }
    }
}

private fun Application.callIdEndpoint() {
    install(CallIdCoroutineContext)
    install(Routing) {
        get("/call-id") {
            call.respond("call id: ${coroutineContext[KtorCallId]?.callId}")
        }
        get("/call-id-other-thread") {
            withContext(otherThreadDispatcher) {
                call.respond("call id: ${coroutineContext[KtorCallId]?.callId}")
            }
        }
    }
}

private val otherThreadDispatcher = Executors.newFixedThreadPool(1).asCoroutineDispatcher()
