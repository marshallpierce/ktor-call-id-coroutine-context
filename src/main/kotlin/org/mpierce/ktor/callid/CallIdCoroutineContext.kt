package org.mpierce.ktor.callid

import io.ktor.application.Application
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.ApplicationFeature
import io.ktor.application.call
import io.ktor.features.callId
import io.ktor.util.AttributeKey
import kotlinx.coroutines.withContext
import kotlin.coroutines.AbstractCoroutineContextElement
import kotlin.coroutines.CoroutineContext

/**
 * Exposes the call id via a [KtorCallId] coroutine context.
 *
 * If the call id is not available, the coroutine context will not be modified.
 *
 * There's no configuration needed; just `install(CallIdCoroutineContext)`.
 */
class CallIdCoroutineContext {
    companion object Feature : ApplicationFeature<Application, Unit, CallIdCoroutineContext> {
        override val key: AttributeKey<CallIdCoroutineContext> = AttributeKey("Call Id Coroutine Context")

        override fun install(pipeline: Application, configure: Unit.() -> Unit): CallIdCoroutineContext {
            val feature = CallIdCoroutineContext()

            // CallId feature inserts a phase earlier
            pipeline.intercept(ApplicationCallPipeline.Features) {
                val id = call.callId

                if (id != null) {
                    withContext(KtorCallId(id)) {
                        proceed()
                    }
                } else {
                    proceed()
                }
            }

            return feature
        }
    }
}

class KtorCallId(val callId: String) : AbstractCoroutineContextElement(KtorCallId) {
    companion object Key : CoroutineContext.Key<KtorCallId>
}
