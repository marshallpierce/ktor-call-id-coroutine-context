 [ ![Download](https://api.bintray.com/packages/marshallpierce/maven/ktor-call-id-coroutine-context/images/download.svg) ](https://bintray.com/marshallpierce/maven/ktor-call-id-coroutine-context/_latestVersion) 

## What is it?

A [Ktor](https://ktor.io/) feature to expose [CallId](https://ktor.io/servers/features/call-id.html) in the coroutine context.

## Why is that useful?

`callId` is exposed as an attribute on the `call`, which is only in scope in the top level of your request handling. However, it's common to delegate to other functions, etc, when handling a request, so that logic wouldn't have access to the call id (for logging, passing as a header to other systems, etc) unless you manually passed it to all those functions. 

This is a perfect use case for coroutine context, though. Suppose you have an endpoint like this:

```kotlin
get("/foo") {
    coroutineScope {
        val otherData = async { loadDataFromAnotherService() }
    
        // ... more logic
    }
}
```

Then, in that function (or anywhere else that shares the coroutine context), you could access the call id:

```kotlin
suspend fun loadDataFromAnotherService(): String {
    val callId = coroutineContext[KtorCallId]?.callId
    
    // use your http client or whatever of of choice
    val serviceRequest = RequestBuilder() 
        .header("x-your-favorite-call-id-header", callId)
        .build()

    // execute the request, etc
    return "some data"
}
```

No need to pass a `callId` parameter around! When it's called in the context of a ktor call, it'll have a call id available, and when it isn't, it won't.

## Usage

In your ktor app setup:

```
install(CallIdCoroutineContext)
```

Then, anywhere in your call handling logic (or any child coroutines):

```
// null if no call id set
val callId = coroutineContext[KtorCallId]?.callId
```
