import java.util.Date

plugins {
    kotlin("jvm") version "1.4.20"
    id("org.jetbrains.dokka") version "1.4.20"
    id("com.github.ben-manes.versions") version "0.36.0"
    `maven-publish`
    id("com.jfrog.bintray") version "1.8.5"
    id("net.researchgate.release") version "2.8.1"
    id("org.jmailen.kotlinter") version "3.3.0"
}


repositories {
    jcenter()
}

group = "org.mpierce.ktor"

val deps by extra {
    mapOf(
        "junit" to "5.7.0",
        "ktor" to "1.5.0"
    )
}

dependencies {
    implementation(kotlin("stdlib"))

    api("io.ktor:ktor-server-core:${deps["ktor"]}")

    testImplementation("io.ktor:ktor-server-test-host:${deps["ktor"]}")


    testImplementation("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
}

tasks {
    test {
        useJUnitPlatform()
    }

    register<Jar>("sourceJar") {
        from(sourceSets["main"].allSource)
        archiveClassifier.set("sources")
    }

    register<Jar>("docJar") {
        from(project.tasks["dokkaHtml"])
        archiveClassifier.set("javadoc")
    }

    afterReleaseBuild {
        dependsOn(bintrayUpload)
    }
}

publishing {
    publications {
        register<MavenPublication>("bintray") {
            from(components["java"])
            artifact(tasks["sourceJar"])
            artifact(tasks["docJar"])
        }
    }

    configure<com.jfrog.bintray.gradle.BintrayExtension> {
        user = rootProject.findProperty("bintrayUser")?.toString()
        key = rootProject.findProperty("bintrayApiKey")?.toString()
        setPublications("bintray")

        with(pkg) {
            repo = "maven"
            setLicenses("Copyfree")
            vcsUrl = "https://bitbucket.org/marshallpierce/ktor-call-id-coroutine-context"
            name = "ktor-call-id-coroutine-context"

            with(version) {
                name = project.version.toString()
                released = Date().toString()
                vcsTag = project.version.toString()
            }
        }
    }
}
